//
//  Animated_Tab_BarApp.swift
//  Animated Tab Bar
//
//  Created by Dmitry Roytman on 01/04/2022.
//

import SwiftUI

@main
struct Animated_Tab_BarApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
