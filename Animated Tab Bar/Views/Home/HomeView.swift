//
//  HomeView.swift
//  Animated Tab Bar
//
//  Created by Dmitry Roytman on 01/04/2022.
//

import SwiftUI

struct HomeView: View {
  @State var selectedTab: String = "house"
  var body: some View {
    ZStack(alignment: .bottom) {
      Color.cyan
      AnimatedTabBar(selectedTab: $selectedTab)
    }
    .ignoresSafeArea()
  }
}

struct HomeView_Previews: PreviewProvider {
  static var previews: some View {
    HomeView()
  }
}
