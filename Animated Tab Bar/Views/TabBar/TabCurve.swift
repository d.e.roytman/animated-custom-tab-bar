//
//  TabCurve.swift
//  Animated Tab Bar
//
//  Created by Dmitry Roytman on 01/04/2022.
//

import SwiftUI

struct TabCurve: Shape {
  var tabPoint: CGFloat
  var animatableData: CGFloat {
    get { tabPoint }
    set { tabPoint = newValue }
  }
  func path(in rect: CGRect) -> Path {
    Path { path in
      path.move(to: .init(x: rect.width, y: rect.height))
      path.addLine(to: .init(x: rect.width, y: .zero))
      path.addLine(to: .zero)
      path.addLine(to: .init(x: .zero, y: rect.height))
      let mid = tabPoint - 15
      path.move(to: .init(x: mid - 40, y: rect.height))
      
      let to = CGPoint(x: mid, y: rect.height - 20)
      let control1 = CGPoint(x: mid - 15, y: rect.height)
      let control2 = CGPoint(x: mid - 15, y: rect.height - 20)
      path.addCurve(to: to, control1: control1, control2: control2)
      
      let to1 = CGPoint(x: mid + 40, y: rect.height)
      let control3 = CGPoint(x: mid + 15, y: rect.height - 20)
      let control4 = CGPoint(x: mid + 15, y: rect.height)
      path.addCurve(to: to1, control1: control3, control2: control4)
    }
  }
}

struct TabCurve_Previews: PreviewProvider {
  static var previews: some View {
    HomeView()
  }
}
