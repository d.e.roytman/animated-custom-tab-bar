//
//  AnimatedTabBar.swift
//  Animated Tab Bar
//
//  Created by Dmitry Roytman on 01/04/2022.
//

import SwiftUI

struct AnimatedTabBar: View {
  @Binding var selectedTab: String
  // Storing each tab bar button mid point
  @State private var tabBarButtonMidpoints: [CGFloat] = []
  var body: some View {
    HStack(spacing: .zero) {
      TabBarButton(imageName: "house", selectedTab: $selectedTab, tabBarButtonMidpoints: $tabBarButtonMidpoints)
      TabBarButton(imageName: "bookmark", selectedTab: $selectedTab, tabBarButtonMidpoints: $tabBarButtonMidpoints)
      TabBarButton(imageName: "message", selectedTab: $selectedTab, tabBarButtonMidpoints: $tabBarButtonMidpoints)
      TabBarButton(imageName: "person", selectedTab: $selectedTab, tabBarButtonMidpoints: $tabBarButtonMidpoints)
    }
    .frame(maxHeight: 50)
    .padding()
    .background(
      Color.white
        .clipShape(
          TabCurve(tabPoint: getTabPoint())
        )
    )
    .overlay(
      Circle()
        .foregroundColor(.white)
        .frame(width: 10, height: 10)
        .offset(x: getTabPoint() - 20),
      alignment: .bottomLeading
    )
    .cornerRadius(20)
    .padding()
  }
  
  private func getTabPoint() -> CGFloat {
    guard tabBarButtonMidpoints.count >= 4 else { return 10 }
    switch selectedTab {
    case "house": return tabBarButtonMidpoints[0]
    case "bookmark": return tabBarButtonMidpoints[1]
    case "message": return tabBarButtonMidpoints[2]
    case "person": return tabBarButtonMidpoints[3]
    default:
      assertionFailure("Unexpected selected tab value <\(selectedTab)>")
      return 10
    }
  }
}

struct AnimatedTabBar_Previews: PreviewProvider {
  static var previews: some View {
    HomeView()
  }
}
