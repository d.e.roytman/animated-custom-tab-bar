//
//  TabBarButton.swift
//  Animated Tab Bar
//
//  Created by Dmitry Roytman on 01/04/2022.
//

import SwiftUI

struct TabBarButton: View {
  let imageName: String
  @Binding var selectedTab: String
  @Binding var tabBarButtonMidpoints: [CGFloat]
  var body: some View {
    Button(
      action: {
        withAnimation(
          .interactiveSpring(response: 0.6, dampingFraction: 0.5, blendDuration: 0.5)
        ) {
          selectedTab = imageName
        }
      }, label: {
        Image(
          systemName:
            imageName == selectedTab
          ? imageName + ".fill"
          : imageName
        )
        .font(.system(size: 25, weight: .semibold))
        .foregroundColor(.primary)
        .offset(y: imageName == selectedTab ? -10 : .zero)
      }
    )
    .frame(maxWidth: .infinity, maxHeight: .infinity)
    .overlay(
      GeometryReader { proxy in
        Spacer()
          .onAppear {
            guard tabBarButtonMidpoints.count <= 4 else { return }
            tabBarButtonMidpoints.insert(
              proxy.frame(in: .global).midX,
              at: .zero
            )
          }
      }
    )
  }
}

struct TabBarButton_Previews: PreviewProvider {
  static var previews: some View {
    Group {
      TabBarButton(imageName: "house", selectedTab: .constant(""), tabBarButtonMidpoints: .constant([]))
      TabBarButton(imageName: "house", selectedTab: .constant("house"), tabBarButtonMidpoints: .constant([]))
    }
  }
}
